package xin.nick;

import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.builder.CustomFile;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 代码生成器支持自定义[DTO\VO等]模版
 */
public final class EnhanceFreemarkerTemplateEngine extends FreemarkerTemplateEngine {
    @Override
    protected void outputCustomFile(List<CustomFile> customFiles,  TableInfo tableInfo,  Map<String, Object> templateMateMap) {


        // controllerMappingHyphen key
        Object controllerMappingHyphen = templateMateMap.get("controllerMappingHyphen");
        if (Objects.nonNull(controllerMappingHyphen)) {

            String controllerMappingString = controllerMappingHyphen.toString();
            // 魔改连字符为 /
//            String controllerMappingHyphenString = controllerMappingString.replaceAll("-", "/");
//
//
//            // 如果模块名和类名一致,则合并前面的名字,
//            // 例如 system/system/user ==> system/user
//            Object config = templateMateMap.get("package");
//            if (Objects.nonNull(config)) {
//                Map<String, Object> configMap = (Map<String, Object>) config;
//                Object moduleName = configMap.get(ConstVal.MODULE_NAME);
//                if (Objects.nonNull(moduleName)) {
//                    String moduleNameString = moduleName.toString();
//                    String moduleNameStringStart = moduleNameString + "/";
//                    // 替换掉模块名
//                    if (controllerMappingHyphenString.startsWith(moduleNameStringStart)) {
//                        controllerMappingHyphenString = controllerMappingHyphenString.substring(moduleNameStringStart.length());
//                    }
//
//                }
//            }
//
//            // 将controller映射字段放进去
//            templateMateMap.put("controllerMappingHyphen", controllerMappingHyphenString);

            // 设置权限编码
            String[] controllerMappingSplitArray = controllerMappingString.split("-");
            String authorityCode = String.join(":", controllerMappingSplitArray);
            templateMateMap.put("authorityCode", authorityCode);
        }

        // 设置自定义类的路径
        String entityName = tableInfo.getEntityName();
        String parentPath = this.getConfigBuilder().getPathInfo().get(OutputFile.parent);
        customFiles.forEach((customFile) -> {

            String currentFileName = customFile.getFileName();
            // DTO,VO,Query 分别放在指定目录
            // parentPath 项目根目录 /xin/nick/
            // currentFileName 是当前循环的文件 例如 VO
            // entityName 是当前的示例名 例如 SystemUser
            // currentFilePath 当前文件目录


            String currentFilePath = currentFileName;
            int i = currentFilePath.lastIndexOf(".");
            if (i != -1) {
                // 文件位置.文件名
                String[] split = currentFilePath.split("\\.");
                currentFilePath = split[0];
                currentFileName = split[1];
            } else {
                currentFileName = currentFilePath.toUpperCase();
            }

            String fileName = parentPath +
                    File.separator + "domain" +
                    File.separator + currentFilePath +
                    File.separator + entityName + currentFileName + ("sql".equals(currentFileName) ? ".sql" : ".java");
            // 设置指定的文件类型,给指定的文件路径
            this.outputFile(new File(fileName), templateMateMap, customFile.getTemplatePath(), false);
        });
    }


//    @Override
//    protected void outputCustomFile( Map<String, String> customFile,  TableInfo tableInfo, Map<String, Object> objectMap) {
//        String entityName = tableInfo.getEntityName();
//        String otherPath = this.getPathInfo(OutputFile.other);
//        customFile.forEach((key, value) -> {
//
//            // DTO,VO,Query 分别放在指定目录
//            String fileName = String.format(otherPath + File.separator + key.toLowerCase() + File.separator + entityName + "%s.java", key);
//            this.outputFile(new File(fileName), objectMap, value, false);
//        });
//    }
}
