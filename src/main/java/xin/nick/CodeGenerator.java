package xin.nick;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import org.apache.ibatis.annotations.Mapper;
import xin.nick.common.mbp.entity.BaseEntity;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * 代码生成器
 * @author Nick
 *  * @see <a href="https://baomidou.com/pages/56bac0/">MyBatis-Plus代码生成器</a>
 *  * @see <a href="https://baomidou.com/pages/981406/">代码生成器配置新</a>
 * @since 2024/4/12
 */
public class CodeGenerator {

    /**
     * 数据库链接信息
     */
    private final static String URL = "jdbc:mysql://127.0.0.1:3306/land_spring_boot_3?useUnicode=true" +
            "&allowPublicKeyRetrieval=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull&useSSL=false&serverTimezone=GMT%2B8";
    private final static String USERNAME = "root";
    private final static String PASSWORD = "root";


    public static void main(String[] args) {

        // 需要生成的表格,用 "," 分割
        String tableList = "system_file";

        // 输入需要过滤掉的表名前缀
        String tablePrefix = "";

        String author = "Nick";

        /**
         * 模块信息
         */
        String packageNme = "xin.nick";
        String moduleName = "system";

        /**
         * 生成路径配置
         */
        String projectPath = "/out/";
        String mappingPath = "/out/mapper/" + moduleName;


//        // 可以指定生成到项目目录下面
        String finalProjectPath = System.getProperty("user.dir") + "/";
        String srcPath = "out/java/";
        String resourcePath = "out/resources/";
        projectPath = finalProjectPath + "/" + srcPath;
        mappingPath = finalProjectPath + "/" + resourcePath + "mapper/" + moduleName;
        System.out.println(finalProjectPath);


        DataSourceConfig.Builder
                dataSourcebuilder = new DataSourceConfig.Builder(URL, USERNAME, PASSWORD);
//        dataSourcebuilder.dbQuery(new PostgreSqlQuery());

        String outProjectPath = projectPath;
        String outMappingPath = mappingPath;
        FastAutoGenerator.create(dataSourcebuilder)
                .globalConfig(config -> {

                    // 设置作者
                    config.author(author)
//                            .enableSwagger()
                            .enableSpringdoc()
                            .disableOpenDir()
                            // 指定输出目录
                            .outputDir(outProjectPath);
                })
                .packageConfig(builder -> {
                    // 设置父包名
                    builder.parent(packageNme)
                            // 设置父包模块名
                            .moduleName(moduleName)
                            // serviceImpl 生成到 service
//                            .serviceImpl("service")
                            // 设置mapperXml生成路径
                            .pathInfo(Collections.singletonMap(OutputFile.xml, outMappingPath))
                    ;
                })
                .strategyConfig(builder -> {
                    // 设置需要生成的表名
                    builder.addInclude(tableList)
                            // 设置过滤表前缀
                            .addTablePrefix(tablePrefix);
                })
                // 2024年6月26日: 还是生成吧,回归正统?
//                .templateConfig(builder -> {
//                    // 模板配置禁止生成service
//                    builder.disable(TemplateType.SERVICE);
//                })
                .strategyConfig(builder -> {

                    // 设置Rest风格的Controller
                    builder.controllerBuilder()
                            // 设置为连字符, 这里魔改了,
                            // 我在 EnhanceFreemarkerTemplateEngine 将它的连字符改为了 斜线 /
                            .enableHyphenStyle()
                            // 设置Rest风格的Controller
                            .enableRestStyle();

                    // 配置Service
//                    builder.serviceBuilder()
//                            .formatServiceImplFileName("%sService")
//                    ;

                    // mapper
                    builder.mapperBuilder().mapperAnnotation(Mapper.class)
                            .formatMapperFileName("%sMapper")
                            .enableBaseResultMap()
                            .enableBaseColumnList();

                    // 设置需要生成的Entity
                    builder.entityBuilder()
                            .enableLombok()
                            .enableRemoveIsPrefix()
                            .disableSerialVersionUID()
                            .enableTableFieldAnnotation()
                            .versionColumnName("version")
                            .versionPropertyName("version")

                            .superClass(BaseEntity.class)
                            .addSuperEntityColumns("created_user_id", "create_time", "updated_user_id", "update_time")
                            .logicDeleteColumnName("deleted")
                            .logicDeletePropertyName("deleted")

                            .enableTableFieldAnnotation().build();
                })


                // 配置生成其他的文件
                .injectionConfig(consumer -> {
                    Map<String, String> customFile = new HashMap<>();
                    // DTO,VO,QO
                    // 文件位置.文件名
                    customFile.put("dto.DTO", "/templates/dto.java.ftl");
//                    customFile.put("dto.CreateDTO", "/templates/create.dto.java.ftl");
//                    customFile.put("dto.UpdateDTO", "/templates/update.dto.java.ftl.20250113");
                    customFile.put("vo.VO", "/templates/vo.java.ftl");
                    customFile.put("query.Query", "/templates/query.java.ftl");
                    customFile.put("sql.sql", "/templates/authority.sql.ftl");
                    customFile.put("enums.ResultCode", "/templates/enums.ResultCode.ftl");
//                    customFile.put("DTO.java", "/templates/dto.java.ftl");
//                    customFile.put("CreateDTO.java", "/templates/create.dto.java.ftl.20250113");
//                    customFile.put("UpdateDTO.java", "/templates/update.dto.java.ftl.20250113");
//                    customFile.put("VO.java", "/templates/vo.java.ftl");
//                    customFile.put("Query.java", "/templates/query.java.ftl");
//                    customFile.put(".sql", "/templates/authority.sql.ftl");
                    consumer.customFile(customFile);


                })
                // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .templateEngine(new EnhanceFreemarkerTemplateEngine())
                .execute();

    }

}
