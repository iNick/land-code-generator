package ${package.Service};

<#assign thisName="${table.comment!}"/>
<#assign thisEntity="${entity?uncap_first}"/>
<#assign thisDTO="${entity}DTO"/>
<#assign thisQuery="${entity}Query"/>
<#assign thisVO="${entity}VO"/>
<#list table.fields as field>
    <#if field.keyFlag>
        <#assign keyPropertyName="${field.propertyName}"/>
    </#if>
</#list>

<#if keyPropertyName??>
    <#assign thisEntityId="${keyPropertyName?cap_first}"/>
<#else>
    <#assign keyPropertyName="id"/>
    <#assign thisEntityId="Id"/>
</#if>
import ${package.Entity}.${entity};
import ${superServiceClassPackage};
import com.baomidou.mybatisplus.core.metadata.IPage;
import ${package.Parent}.domain.dto.${entity}DTO;
import ${package.Parent}.domain.query.${thisQuery};
import ${package.Parent}.domain.vo.${thisVO};

import java.util.List;

/**
 * <p>
 * ${thisName} 服务类接口
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {


    /**
     * 获取 ${thisName} 详情
     * @param ${keyPropertyName} 主键
     * @return ${thisVO}
     */
    ${thisVO} getDetail(Long ${keyPropertyName});

    /**
     * 获取 ${thisName}列表
     * @param query 查询条件
     * @return List<${thisVO}>
     */
    List<${thisVO}> getList(${thisQuery} query);

    /**
     * 获取 ${thisName} 列表 分页
     * @param query 查询条件
     * @return IPage<${thisVO}>
     */
    IPage<${thisVO}> getListPage(${thisQuery} query);

    /**
     * ${thisName} 添加
     * @param dto 新增对象dto
     * @return 展示对象VO
     */
    ${thisVO} create${entity}(${entity}DTO dto);

    /**
     * ${thisName} 更新
     * @param dto 更新对象dto
     * @return ${thisVO}
     */
    ${thisVO} update${entity}(${entity}DTO dto);

    /**
     * ${thisName} 根据主键删除
     * @param ${keyPropertyName} 主键
     */
    void deleteById(Long ${keyPropertyName});

    /**
     * ${thisName} 根据 ${keyPropertyName} 列表 批量删除
     * @param ${keyPropertyName}List ${keyPropertyName} 列表
     */
    void deleteByIdList(List<Long> ${keyPropertyName}List);

}
</#if>
