package ${package.Controller};

<#assign thisName="${table.comment!}"/>
<#assign thisEntity="${entity?uncap_first}"/>
<#assign thisDTO="${entity}DTO"/>
<#assign thisQuery="${entity}Query"/>
<#assign thisVO="${entity}VO"/>
<#list table.fields as field>
    <#if field.keyFlag>
        <#assign keyPropertyName="${field.propertyName}"/>
    </#if>
</#list>

<#if keyPropertyName??>
    <#assign thisEntityId="${keyPropertyName?cap_first}"/>
<#else>
    <#assign keyPropertyName="id"/>
    <#assign thisEntityId="Id"/>
</#if>
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import xin.nick.common.core.constant.SystemConstant;
import xin.nick.common.core.entity.DataOperation;
import ${package.Service}.I${entity}Service;
import ${package.Parent}.domain.dto.${entity}DTO;
import ${package.Parent}.domain.query.${thisQuery};
import ${package.Parent}.domain.vo.${thisVO};
import xin.nick.common.core.annotation.RequestLog;
import xin.nick.common.core.annotation.UnifiedResult;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

import java.util.List;

/**
 * <p>
 * ${thisName} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Tag(name = "<#if controllerMappingHyphenStyle>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
@UnifiedResult
@RequestLog
@RequiredArgsConstructor
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName?? && package.ModuleName != "">/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>

    <#assign thisService="${entity?uncap_first}Service"/>
    private final I${entity}Service ${thisService};

    @Operation(summary = "${thisName}详情",
            security = { @SecurityRequirement(name = SystemConstant.HEADER_AUTHORIZATION) })
    @GetMapping("/getDetail/{${keyPropertyName}}")
    @PreAuthorize("hasRole('" + SystemConstant.ROOT + "') || hasAnyAuthority('<#if package.ModuleName?? && package.ModuleName != "">${package.ModuleName}:</#if><#if controllerMappingHyphenStyle>${controllerMappingHyphen}<#else>${table.entityPath}</#if>:detail')")
    public ${thisVO} getDetail(@PathVariable(name = "${keyPropertyName}") Long ${keyPropertyName}) {
        return ${thisService}.getDetail(${keyPropertyName});
    }

    @Operation(summary = "${thisName}列表",
            security = { @SecurityRequirement(name = SystemConstant.HEADER_AUTHORIZATION) })
    @GetMapping("/getList")
    @PreAuthorize("hasRole('" + SystemConstant.ROOT + "') || hasAnyAuthority('<#if package.ModuleName?? && package.ModuleName != "">${package.ModuleName}:</#if><#if controllerMappingHyphenStyle>${controllerMappingHyphen}<#else>${table.entityPath}</#if>:list')")
    public List<${thisVO}> getList(@ParameterObject ${thisQuery} query) {
        return ${thisService}.getList(query);
    }

    @Operation(summary = "${thisName}列表分页",
            security = { @SecurityRequirement(name = SystemConstant.HEADER_AUTHORIZATION) })
    @GetMapping("/getListPage")
    @PreAuthorize("hasRole('" + SystemConstant.ROOT + "') || hasAnyAuthority('<#if package.ModuleName?? && package.ModuleName != "">${package.ModuleName}:</#if><#if controllerMappingHyphenStyle>${controllerMappingHyphen}<#else>${table.entityPath}</#if>:list')")
    public IPage<${thisVO}> getListPage(@ParameterObject ${thisQuery} query) {
        return ${thisService}.getListPage(query);
    }

    @Operation(summary = "${thisName}创建",
            security = { @SecurityRequirement(name = SystemConstant.HEADER_AUTHORIZATION) })
    @PostMapping("/create")
    @PreAuthorize("hasRole('" + SystemConstant.ROOT + "') || hasAnyAuthority('<#if package.ModuleName?? && package.ModuleName != "">${package.ModuleName}:</#if><#if controllerMappingHyphenStyle>${controllerMappingHyphen}<#else>${table.entityPath}</#if>:create')")
    public ${thisVO} create(@RequestBody @Validated(value = { DataOperation.Create.class}) ${entity}DTO dto) {
        return ${thisService}.create${entity}(dto);
    }

    @Operation(summary = "${thisName}更新",
            security = { @SecurityRequirement(name = SystemConstant.HEADER_AUTHORIZATION) })
    @PutMapping("/update")
    @PreAuthorize("hasRole('" + SystemConstant.ROOT + "') || hasAnyAuthority('<#if package.ModuleName?? && package.ModuleName != "">${package.ModuleName}:</#if><#if controllerMappingHyphenStyle>${controllerMappingHyphen}<#else>${table.entityPath}</#if>:update')")
    public ${thisVO} update(@RequestBody @Validated(value = { DataOperation.Update.class}) ${entity}DTO dto) {
        return ${thisService}.update${entity}(dto);
    }

    @Operation(summary = "${thisName}删除",
            security = { @SecurityRequirement(name = SystemConstant.HEADER_AUTHORIZATION) })
    @DeleteMapping("/delete/{${keyPropertyName}}")
    @PreAuthorize("hasRole('" + SystemConstant.ROOT + "') || hasAnyAuthority('<#if package.ModuleName?? && package.ModuleName != "">${package.ModuleName}:</#if><#if controllerMappingHyphenStyle>${controllerMappingHyphen}<#else>${table.entityPath}</#if>:delete')")
    public void delete(@PathVariable(name = "${keyPropertyName}") Long ${keyPropertyName}) {
        ${thisService}.deleteById(${keyPropertyName});
    }

    @Operation(summary = "${thisName}批量删除",
            security = { @SecurityRequirement(name = SystemConstant.HEADER_AUTHORIZATION) })
    @DeleteMapping("/deleteByIdList/{${keyPropertyName}List}")
    @PreAuthorize("hasRole('" + SystemConstant.ROOT + "') || hasAnyAuthority('<#if package.ModuleName?? && package.ModuleName != "">${package.ModuleName}:</#if><#if controllerMappingHyphenStyle>${controllerMappingHyphen}<#else>${table.entityPath}</#if>:delete')")
    public void delete(@PathVariable(name = "${keyPropertyName}List") List<Long> ${keyPropertyName}List) {
        ${thisService}.deleteByIdList(${keyPropertyName}List);
    }

}
</#if>
