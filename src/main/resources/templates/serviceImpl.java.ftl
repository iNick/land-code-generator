package ${package.ServiceImpl};

<#assign thisName="${table.comment!}"/>
<#assign thisEntity="${entity?uncap_first}"/>
<#assign thisDTO="${entity}DTO"/>
<#assign thisQuery="${entity}Query"/>
<#assign thisVO="${entity}VO"/>
<#list table.fields as field>
    <#if field.keyFlag>
        <#assign keyPropertyName="${field.propertyName}"/>
    </#if>
</#list>

<#if keyPropertyName??>
    <#assign thisEntityId="${keyPropertyName?cap_first}"/>
<#else>
    <#assign keyPropertyName="id"/>
    <#assign thisEntityId="Id"/>
</#if>
<#assign underlineCaseEntity="${entity?replace(\"([a-z])([A-Z]+)\",\"$1_$2\",\"r\")?upper_case}"/>
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${superServiceImplClassPackage};
import ${package.Service}.${table.serviceName};
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import ${package.Parent}.domain.dto.${entity}DTO;
import ${package.Parent}.domain.query.${thisQuery};
import ${package.Parent}.domain.vo.${thisVO};
import xin.nick.common.core.util.BeanCopierUtil;
import xin.nick.common.core.util.MyAssert;
import xin.nick.system.domain.enums.${entity}ResultCode;

import java.util.List;

/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#assign thisService="${entity?uncap_first}Service"/>
@Service(value = "${thisService}")
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}> implements ${table.serviceName} {


    /**
     * 获取 ${thisName} 详情
     * @param ${keyPropertyName} 主键
     * @return ${thisVO}
     */
    @Override
    public ${thisVO} getDetail(Long ${keyPropertyName}) {
        MyAssert.notNull(${keyPropertyName}, ${entity}ResultCode.${underlineCaseEntity}_ID_IS_EMPTY);
        ${entity} ${thisEntity} = getById(${keyPropertyName});
        MyAssert.notNull(${thisEntity}, ${entity}ResultCode.${underlineCaseEntity}_IS_EMPTY);
        return BeanCopierUtil.copyProperties(${thisEntity}, ${thisVO}.class);
    }

    /**
     * 获取 ${thisName} 列表
     * @param query 查询条件对象
     * @return List<${thisVO}>
     */
    @Override
    public List<${thisVO}> getList(${thisQuery} query) {

        LambdaQueryWrapper<${entity}> queryWrapper = getListQueryWrapper(query);

        List<${entity}> ${thisEntity}List = list(queryWrapper);

        return BeanCopierUtil.copyToList(${thisEntity}List, ${thisVO}.class);

    }

    /**
     * 获取 ${thisName} 列表分页
     * @param query 查询条件对象
     * @return Page<${thisVO}>
     */
    @Override
    public IPage<${thisVO}> getListPage(${thisQuery} query) {

        LambdaQueryWrapper<${entity}> queryWrapper = getListQueryWrapper(query);

        IPage<${entity}> page = page(new Page<>(query.getCurrent(), query.getSize()), queryWrapper);

        return page.convert(entity -> BeanCopierUtil.copyProperties(entity, ${thisVO}.class));
    }

    /**
     * ${thisName} 添加
     * @param dto 创建对象dto
     * @return ${thisVO}
     */
    @Override
    public ${thisVO} create${entity}(${entity}DTO dto) {
        ${entity} ${thisEntity} = BeanCopierUtil.copyProperties(dto, ${entity}.class);
        ${thisEntity}.set${thisEntityId}(null);
        boolean insertStatus = SqlHelper.retBool(baseMapper.insert(${thisEntity}));
        MyAssert.isTrue(insertStatus, "新增失败");
        return getDetail(${thisEntity}.get${thisEntityId}());
    }

    /**
     * ${thisName} 更新
     * @param dto 更新对象dto
     * @return ${thisVO}
     */
    @Override
    public ${thisVO} update${entity}(${entity}DTO dto) {
        MyAssert.notNull(dto.get${thisEntityId}(), ${entity}ResultCode.${underlineCaseEntity}_ID_IS_EMPTY);
        ${entity} ${thisEntity} = BeanCopierUtil.copyProperties(dto, ${entity}.class);
        boolean updateStatus = SqlHelper.retBool(baseMapper.updateById(${thisEntity}));
        MyAssert.isTrue(updateStatus, "更新失败");
        return getDetail(${thisEntity}.get${thisEntityId}());
    }


    /**
     * ${thisName} 删除
     * @param ${keyPropertyName} 主键信息
     */
    @Override
    public void deleteById(Long ${keyPropertyName}) {
        MyAssert.notNull(${keyPropertyName}, ${entity}ResultCode.${underlineCaseEntity}_ID_IS_EMPTY);
        boolean deleteStatus = SqlHelper.retBool(baseMapper.deleteById(${keyPropertyName}));
        MyAssert.isTrue(deleteStatus, "删除失败");
    }


    /**
     * ${thisName} 根据 ${keyPropertyName} 列表 批量删除
     * @param ${keyPropertyName}List ${keyPropertyName} 列表
     */
    @Override
    public void deleteByIdList(List<Long> ${keyPropertyName}List) {
        if (CollUtil.isEmpty(${keyPropertyName}List)) {
            return;
        }
        boolean deleteStatus = SqlHelper.retBool(baseMapper.deleteByIds(${keyPropertyName}List));
        MyAssert.isTrue(deleteStatus, "删除失败");
    }


    /**
     * 获取列表查询条件
     *
     * @param query 查询条件
     * @return LambdaQueryWrapper 条件查询包装类
     */
    private LambdaQueryWrapper<${entity}> getListQueryWrapper(${thisQuery} query) {

        ${entity} ${thisEntity} = BeanCopierUtil.copyProperties(query, ${entity}.class);
        return new LambdaQueryWrapper<>(${thisEntity});

    }

}
</#if>
