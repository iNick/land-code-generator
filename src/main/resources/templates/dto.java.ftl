package ${package.Parent}.domain.dto;

<#if springdoc>
import io.swagger.v3.oas.annotations.media.Schema;
<#elseif swagger>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import xin.nick.common.core.entity.DataOperation;
<#if entityLombokModel>
import lombok.Data;
    <#if chainModel>
import lombok.experimental.Accessors;
    </#if>
</#if>

import java.time.LocalDateTime;
import java.math.BigDecimal;

/**
 * <p>
 * ${table.comment!}
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if entityLombokModel>
@Data
    <#if chainModel>
@Accessors(chain = true)
    </#if>
</#if>
<#if springdoc>
@Schema(name = "${entity}", description = "${table.comment}")
<#elseif swagger>
@ApiModel(value = "${entity}对象", description = "${entity}对象-${table.comment!}")
</#if>
<#if entitySerialVersionUID>
public class ${entity}DTO implements Serializable {
<#else>
public class ${entity}DTO {
</#if>
<#if entitySerialVersionUID>

    private static final long serialVersionUID = 1L;
</#if>
<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list table.fields as field>
    <#if field.keyFlag>
        <#assign keyPropertyName="${field.propertyName}"/>
    </#if>

    <#if field.comment!?length gt 0>
        <#if springdoc>
    @Schema(description = "${field.comment}")
        <#elseif swagger>
    @ApiModelProperty("${field.comment}")
        <#else>
            /**
            * ${field.comment}
            */
        </#if>
    </#if>
    <#-- 判断是否必填 -->
    <#if field.metaInfo.nullable == false>
        <#if field.metaInfo.jdbcType == "VARCHAR" >
    @NotBlank(message = "[${field.comment}]不可为空"<#if field.keyFlag>, groups = {DataOperation.Update.class}</#if>)
        <#else>
    @NotNull(message = "[${field.comment}]不可为空"<#if field.keyFlag>, groups = {DataOperation.Update.class}</#if>)
        </#if>
    </#if>
    <#-- 限制最大长度 -->
    <#-- <#if field.metaInfo.jdbcType == "BIGINT"
            || field.metaInfo.jdbcType == "INTEGER">
    @Max(value = ${field.metaInfo.length}, message = "[${field.comment}]不可以超过${field.metaInfo.length}")
    </#if> -->
    <#if field.metaInfo.jdbcType == "VARCHAR">
    @Size(max = ${field.metaInfo.length}, message = "[${field.comment}]不可以超过${field.metaInfo.length}")
    </#if>
    <#if field.propertyType = "LocalDateTime">
    @JsonFormat(pattern = "yyyy-MM-dd  HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    </#if>
    private ${field.propertyType} ${field.propertyName};
</#list>
<#------------  END 字段循环遍历  ---------->

<#if !entityLombokModel>
    <#list table.fields as field>
        <#if field.propertyType == "boolean">
            <#assign getprefix="is"/>
        <#else>
            <#assign getprefix="get"/>
        </#if>
    public ${field.propertyType} ${getprefix}${field.capitalName}() {
        return ${field.propertyName};
    }

    <#if chainModel>
    public ${entity} set${field.capitalName}(${field.propertyType} ${field.propertyName}) {
    <#else>
    public void set${field.capitalName}(${field.propertyType} ${field.propertyName}) {
    </#if>
        this.${field.propertyName} = ${field.propertyName};
        <#if chainModel>
        return this;
        </#if>
    }
    </#list>
</#if>

<#if entityColumnConstant>
    <#list table.fields as field>
    public static final String ${field.name?upper_case} = "${field.name}";

    </#list>
</#if>
<#if activeRecord>
    @Override
    public Serializable pkVal() {
    <#if keyPropertyName??>
        return this.${keyPropertyName};
    <#else>
        return null;
    </#if>
    }

</#if>
<#if !entityLombokModel>
    @Override
    public String toString() {
        return "${entity}{" +
    <#list table.fields as field>
        <#if field_index==0>
            "${field.propertyName}=" + ${field.propertyName} +
        <#else>
            ", ${field.propertyName}=" + ${field.propertyName} +
        </#if>
    </#list>
        "}";
    }
</#if>
}
