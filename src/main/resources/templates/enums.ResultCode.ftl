package ${package.Parent}.domain.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import xin.nick.common.core.enums.ExceptionTypeEnum;
import xin.nick.common.core.enums.IResultCodeEnum;

<#assign underlineCaseEntity="${entity?replace(\"([a-z])([A-Z]+)\",\"$1_$2\",\"r\")?upper_case}"/>
<#list table.fields as field>
    <#if field.keyFlag>
        <#assign keyPropertyName="${field.propertyName}"/>
    </#if>
</#list>

<#if keyPropertyName??>
    <#assign thisEntityId="${keyPropertyName?cap_first}"/>
<#else>
    <#assign keyPropertyName="id"/>
    <#assign thisEntityId="Id"/>
</#if>

/**
 * <p>
 * ${table.comment!} 响应错误码
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum ${entity}ResultCode implements IResultCodeEnum {


    ${underlineCaseEntity}_ID_IS_EMPTY(ExceptionTypeEnum.PARAM, "${underlineCaseEntity}_ID_IS_EMPTY", "[${keyPropertyName}]为空"),
    ${underlineCaseEntity}_IS_EMPTY(ExceptionTypeEnum.CUSTOM, "${underlineCaseEntity}_IS_EMPTY", "[${table.comment!}]不存在"),
    ;

    /**
     * 提示的异常类型,用于 MyAssert 返回异常类型的判断
     */
    private ExceptionTypeEnum exceptionType;

    /**
     * 提示内容
     */
    @JsonValue
    private String value;

    /**
     * 描述
     */
    private String description;


}

