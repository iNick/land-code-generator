# 懒得写代码

land-code-generator

用MyBatisPlus的代码生成器改的,

从项目 Land 改吧改吧来的:

https://gitee.com/iNick/land

## 启动

找到 CodeGenerator 启动

[CodeGenerator.java](src%2Fmain%2Fjava%2Fxin%2Fnick%2FCodeGenerator.java)

里面修改数据库地址和数据表就好

## 其他

代码提交说明规范

``` text
feat： 新增 feature
fix: 修复 bug
docs: 仅仅修改了文档，比如 README, CHANGELOG, CONTRIBUTE等等
style: 仅仅修改了空格、格式缩进、逗号等等，不改变代码逻辑
refactor: 代码重构，没有加新功能或者修复 bug
perf: 优化相关，比如提升性能、体验
test: 测试用例，包括单元测试、集成测试等
chore: 改变构建流程、或者增加依赖库、工具等
revert: 回滚到上一个版本
init: 初始化
build: 构建项目
scope: commit 影响的范围, 比如: route, component, utils, build…
subject: commit 的概述
```

